  $ cat << EOF | ./REPL.exe -
  > (z x)
  > EOF
  Result: (z x)
  $ cat | ./REPL.exe - <<EOF
  > fun x->x
  > EOF
  Result: (λ x -> x)
  $ cat | ./REPL.exe - <<EOF
  > fun f -> (fun x -> f (x x)) (fun x -> f (x x))
  > EOF
  Result: (λ f -> ((λ x -> (f (x x))) (λ x -> (f (x x)))))
  $ cat | ./REPL.exe - <<EOF
  > (fun x . fun y . x) (fun x . x)
  > EOF
  Result: ⊥
  $ cat | ./REPL.exe - <<EOF
  > (fun x . fun y . x) (fun x . x) x
  > EOF
  Result: (λ _x -> _x)
  $ echo '(fac 6)'
  (fac 6)
  $ cat | ./REPL.exe -no - <<EOF
  > (λ f -> ((λ x -> (f (x x))) (λ x -> (f (x x)))))
  >   (λ self -> fun nn ->
  >      (((λ n -> ((n (λ uu -> fun vv -> fun y -> y)) (fun f -> fun x -> f))) nn) (fun f -> fun x -> f x))
  >      ( (λ x -> fun y -> fun z -> x (y z))
  >         (self ( (λ n -> fun f -> fun x ->
  >                  (n (λ g -> fun h -> h (g f))) (λ uuu -> x) (λ u -> u) )
  >               nn)) nn)
  >   )
  >   (λ f -> fun x -> f (f (f x)))
  > EOF
  Result: (λ z x -> (z (z (z (z (z (z x)))))))
