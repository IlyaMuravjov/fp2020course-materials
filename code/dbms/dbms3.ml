open Common

module RAISE (M : MONADFAIL) : sig
  include TRANSFORMER with type 'a m = 'a M.t
  include MONADFAIL with type 'a t := 'a t
end = struct
  (*
  Законы R1
  Законы O3
*)
  type 'a m = 'a M.t

  type 'a t =
    | Return : 'a -> 'a t
    | Bind : 'b t * ('b -> 'a t) -> 'a t
    | Raise : exc -> 'a t
    | Promote : 'a m -> 'a t

  let promote m = Promote m
  let return x = Return x
  let ( >>= ) x f = Bind (x, f)
  let fail e = Raise e

  let observe = function
    | Return a -> M.return a
    | Raise exc -> failwith exc
    | Promote m -> m
    | Bind (_x, _f) -> failwith "???"

  (* Старая реализация *)
  let rec observe = function
    | Return a -> M.return a
    | Raise exc -> M.fail exc
    | Promote m -> m
    | Bind (Return a, k) -> observe (k a)
    | Bind (Raise s, _) -> M.fail s
    | Bind (Promote m, k) -> M.( >>= ) m (fun x -> observe (k x))
    | Bind (Bind (m, k1), k2) -> observe (Bind (m, fun a -> k1 a >>= k2))

  (* выделяем обработку Bind  в отдельную функцию observe1 *)
  let rec observe : 'a t -> 'a m = function
    | Bind (op, c) -> observe1 op c
    | Return x -> M.return x
    | Raise e -> M.fail e
    | Promote m -> m

  and observe1 : 'b. 'b t -> ('b -> 'a t) -> 'a m =
   fun op c ->
    match op with
    | Bind (m, k) -> observe1 m (fun a -> Bind (k a, c))
    | Raise e -> M.fail e
    | Return a ->
        (*** смотрим сюда  *)
        observe (c a)
    | Promote m ->
        (*** и сюда *)
        M.( >>= ) m (fun a -> observe (c a))

  (* CPS implementation *)
  (* Тут мы обнаруживаем, что `observe` вызывается всегда рядом с `с` *)
  (* Идея: давайте передавать `(fun a -> observe (c a))`  вместо `c`

     forall a . c2 a === observe (c a)                                 (1)
       ==>
         observe2 op c2 === observe (Bind (op, c))
  *)

  (* Надо как-то вывести реализацию observe2, например от Return a

      observe2 (Return a) c2
        === spec & assumption by (1)
      observe (Bind (Return a, c2))
        === по самой первой реализации (определению) оbserve
      observe (c2 a)
        ===  (1)
      с2 a
  *)

  (* для op=Raise и op=Promote m аналогично  *)

  (* Теперь для Bind (m,k)

      observe2 (Bind (m,k)) c2
        === spec & assumption by (1)
      observe (Bind (Bind(m,k), c))
        === по самой первой реализации (определению) оbserve
      observe (Bind (m, fun a -> Bind (k a, c) ))
        === spec
      observe2 m (fun a -> observe (Bind (k a, c)) )
        === spec & assumption by (1)
      observe2 m (fun a -> observe2 (k a) c2)
  *)

  let rec observe2 : 'a 'b. 'a t -> ('a -> 'b M.t) -> 'b M.t =
   fun op c2 ->
    match op with
    | Bind (m, k) -> observe2 m (fun a -> observe2 (k a) c2)
    | Return a -> c2 a
    | Raise e -> M.fail e
    | Promote m -> M.( >>= ) m c2

  let rec observe = function
    | Return a -> M.return a
    | Raise exc -> failwith exc
    | Promote m -> m
    | Bind (m, f) -> observe2 m (fun x -> observe (f x))
end
[@@warning "-37-32"]

(* TODO: пример как можно что-нибудь запустить *)
