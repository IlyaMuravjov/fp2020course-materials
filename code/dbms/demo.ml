open Common

(* Demo *)

module Demo1 (Impl : MAKE_TRANSFORMER) (M : MONADFAIL) = struct
  module T = Impl (M)
  open T

  let inc n = promote (M.return (n + 1))
  let _ = observe (promote (M.return 0) >>= inc >>= inc >>= inc)

  let hundred_incs init =
    let rec helper acc n =
      if n >= 100 then acc else helper (acc >>= inc) (n + 1) in
    helper (promote (M.return init)) 0

  let run () = observe (hundred_incs 0)

  (* Если потом это потестить, то оно должно тормозить *)
end

open Benchmark

let () =
  let wrap tr () =
    let (module T : MAKE_TRANSFORMER) = tr in
    let module M = Demo1 (T) (Identity) in
    let ans = M.run () in
    (* Format.printf "%d\n%!" ans; *)
    assert (100 = ans) in
  tabulate
  @@ throughputN ~repeat:1 1
       [ ("1", wrap (module Dbms1.RAISE : MAKE_TRANSFORMER), ())
       ; ("2", wrap (module Dbms2.RAISE : MAKE_TRANSFORMER), ())
       ; ("3", wrap (module Dbms3.RAISE : MAKE_TRANSFORMER), ()) ]
