open Common

module RAISE (M : MONADFAIL) : sig
  include TRANSFORMER with type 'a m = 'a M.t
  include MONADFAIL with type 'a t := 'a t
end = struct
  (*
  Законы R1
  Законы O3
*)
  type 'a m = 'a M.t

  type 'a t =
    | Return : 'a -> 'a t
    | Bind : 'b t * ('b -> 'a t) -> 'a t
    | Raise : exc -> 'a t
    | Promote : 'a m -> 'a t

  let promote m = Promote m
  let return x = Return x
  let ( >>= ) x f = Bind (x, f)
  let fail x = Raise x

  let observe = function
    | Return a -> M.return a
    | Raise exc -> failwith exc
    | Promote m -> m
    | Bind (_x, _f) -> failwith "???"

  let rec observe = function
    | Return a -> M.return a
    | Raise exc -> M.fail exc
    | Promote m -> m
    | Bind (Return a, k) -> observe (k a)
    | Bind (Raise s, _) -> M.fail s
    | Bind (Promote m, k) -> M.( >>= ) m (fun x -> observe (k x))
    | Bind (Bind (m, k1), k2) -> observe (Bind (m, fun a -> k1 a >>= k2))

  (* Эта штука завершается, потому что первый аргумент Bind становится меньше *)

  (* Формально Bind (Return a, return) =/= Return a
       но с точки зрения наблюдателя -- да
     observe (Bind (Return a, return)) === observe (Return a)
  *)

  module N : sig end = struct
    (* Переход к следующему шагу *)
    let rec simplifier : 'a t -> 'a t = function
      | Return a -> Return a
      | Bind (Return a, k) -> simplifier (k a)
      | Bind (Bind (m, k1), k2) -> simplifier (m >>= fun a -> k1 a >>= k2)
      | Raise exc | Bind (Raise exc, _) -> Raise exc
      | Promote m ->
          Promote m (* Что эквивалентно promote m >>= return *)
      | Bind (Promote m, k) -> Promote m >>= fun x -> simplifier (k x)
  end
end
[@@warning "-37-32"]

(* TODO: пример как можно что-нибудь запустить *)
