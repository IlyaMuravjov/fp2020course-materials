module _ = struct
  let fac =
    let rec helper acc i = if i <= 1 then acc else helper (acc * i) (i - 1) in
    helper 1
  ;;

  let%test _ = fac 5 = 120

  let rec fac_non_tailrec i = if i <= 1 then 1 else fac_non_tailrec (i - 1) * i

  let%test _ = fac_non_tailrec 5 = 120

  module WithFixPoint = struct
    let fix f = (fun x -> f (fun v -> x x v)) (fun x -> f (fun v -> x x v))
    let rec fix f x = f (fix f) x
    let fac2 self n = if n <= 1 then 1 else n * self (n - 1)

    let%test _ = fix fac2 5 = 120
  end
end

let rec fib_rec n = if n <= 1 then 1 else fib_rec (n - 1) + fib_rec (n - 2)
let fib_open self n = if n <= 1 then 1 else self (n - 1) + self (n - 2)

let%test _ = fib_rec 5 = 8

(* memoization for non-recursive functions *)
let memoize f =
  let open Base in
  let table = Hashtbl.Poly.create () in
  let g x =
    match Hashtbl.find table x with
    | Some y -> y
    | None ->
      let y = f x in
      Hashtbl.add_exn table ~key:x ~data:y;
      y
  in
  g
;;

(* memoization for open recursive functions *)
let memo_rec1 f_open_rec =
  (* Eta expansion hurts performance *)
  let rec f_rec_memo eta = memoize (fun x -> f_open_rec f_rec_memo x) eta in
  f_rec_memo
;;

let memo_rec2 f_open_rec =
  (* using mutation we can put configurable function here *)
  let f = ref (fun _ -> assert false) in
  let f_rec_memo = memoize (fun x -> f_open_rec !f x) in
  f := f_rec_memo;
  f_rec_memo
;;

(* memoization for open recursion *)
let memo_rec3 f =
  let h = Stdlib.Hashtbl.create 11 in
  let rec g x =
    try Stdlib.Hashtbl.find h x with
    | Stdlib.Not_found ->
      let y = f g x in
      Stdlib.Hashtbl.add h x y;
      y
  in
  g
;;

let () = assert (fib_rec 2 = 2)
