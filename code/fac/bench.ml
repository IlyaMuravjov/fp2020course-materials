open Benchmark
open Lib

let __ () =
  let f = memoize fib_rec in
  Printf.printf "%s %d\n%!" __FILE__ __LINE__;
  ignore (f 45);
  Printf.printf "%s %d\n%!" __FILE__ __LINE__;
  ignore (f 45);
  Printf.printf "%s %d\n%!" __FILE__ __LINE__
;;

let () =
  let res =
    throughputN
      ~repeat:5
      1
      [ "memoized1 fib_open", memo_rec1 fib_open, 10
      ; "recursive  fib_rec", fib_rec, 10
      ; "memoized2 fib_open", memo_rec2 fib_open, 10
      ; "memoized3 fib_open", memo_rec3 fib_open, 10
      ]
  in
  tabulate res
;;
