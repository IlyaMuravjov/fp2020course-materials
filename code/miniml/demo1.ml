open Miniml
open Parsetree

let __ () =
  let open Parsing in
  match parse "1" with
  | Result.Ok e -> Format.printf "%a\n%!" pp_expr e
  | Error s -> Format.eprintf "Parsing error: %s\n%!" s
;;

(* let __ () =
  let open Parsing in
  match Angstrom.parse_string ~consume:All Parsing.(typ) " int -> int" with
  | Result.Ok e -> Format.printf "%a\n%!" pp_typ e
  | Error s -> Format.eprintf "Parsing error: %s\n%!" s
;; *)

let wrap_e s =
  let ast = Base.Result.ok_or_failwith @@ Parsing.parse s in
  print_endline @@ Parsetree.show_expr ast;
  match Inferencer.w ast with
  | Result.Ok t -> Format.printf "type is %a\n%!" Pprint.pp_typ t
  | Error e -> Format.printf "Error: %a\n%!" Inferencer.pp_error e
;;

let __ _ = wrap_e "1+2"
let __ _ = wrap_e "let i = fun x -> x in i"
let __ _ = wrap_e "let a = fun f -> fun x -> f x in a"
let __ _ = wrap_e "(fun x -> x)2"
let _ = wrap_e "let rec f = fun n -> f in f"
