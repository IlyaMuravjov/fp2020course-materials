open Format
open Typedtree

let rec pp_typ ppf = function
  | V n -> fprintf ppf "'_%d" n
  | Prim s -> pp_print_string ppf s
  | Arrow (l, r) -> fprintf ppf "(%a -> %a)" pp_typ l pp_typ r
;;

let pp_scheme ppf = function
  | S (xs, t) -> fprintf ppf "forall %a . %a" VarSet.pp xs pp_typ t
;;

let pp_expr =
  let open Parsetree in
  let rec helper ppf = function
    | EConst n -> fprintf ppf "%d" n
    | EIf (c, th, el) -> fprintf ppf "if %a then %a else %a" helper c helper th helper el
    | EVar s -> pp_print_string ppf s
    | EApp (l, r) -> fprintf ppf "(%a %a)" helper l helper r
    | ELam (PVar name, e) -> fprintf ppf "(fun %s -> %a)" name helper e
    | ELet (_flg, PVar name, body, in_) ->
      fprintf ppf "let %s%s = %a in %a" "?" name helper body helper in_
  in
  helper
;;
