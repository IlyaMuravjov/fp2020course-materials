open Angstrom
open Parsetree

let use_logging = false

let log fmt =
  if use_logging
  then Format.kasprintf (fun s -> Format.printf "%s\n%!" s) fmt
  else Format.ifprintf Format.std_formatter fmt
;;

let pp_list f ppf xs =
  Format.pp_print_list ~pp_sep:(fun ppf () -> Format.fprintf ppf " ") f ppf xs
;;

let ws =
  skip_while (function
      | '\x20' | '\x0a' | '\x0d' | '\x09' -> true
      | _ -> false)
;;

let trace_pos msg =
  let* n = pos in
  log "`%s` on pos %d" msg n;
  return ()
;;

let trace_avai msg =
  available
  >>= fun n ->
  log "`%s` there are %d available." msg n;
  return ()
;;

let lchar c = ws *> char c
let parens p = char '(' *> trace_pos "after(" *> p <* trace_pos "before ')'" <* lchar ')'
let const = char '0' >>= fun c -> return (Printf.sprintf "%c" c)

type dispatch =
  { prio : dispatch -> expr t
  ; expr : dispatch -> expr t
  ; expr_long : dispatch -> expr t
  }

let is_digit = function
  | '0' .. '9' -> true
  | _ -> false
;;

let to_digit c = Char.code c - Char.code '0'

let digit =
  any_char
  >>= function
  | '0' .. '9' as c -> return (Char.code c - Char.code '0')
  | _ -> fail ""
;;

let number =
  trace_pos "number" *> digit
  >>= fun h ->
  scan_state h (fun st c -> if is_digit c then Some ((10 * st) + to_digit c) else None)
;;

let is_alpha = function
  | 'a' .. 'z' | 'A' .. 'Z' -> true
  | _ -> false
;;

let alpha_c =
  any_char
  >>= function
  | c -> if is_alpha c then return c else fail (Format.sprintf "'%c' not a letter" c)
;;

let is_keyword = function
  | "fun" | "in" | "let" | "rec" | "if" | "then" | "else" -> true
  | _ -> false
;;

let ident =
  let* i =
    let* h = ws *> alpha_c in
    let* tl = many alpha_c in
    return (Base.String.of_char_list (h :: tl))
  in
  if is_keyword i
  then fail "got a keyword"
  else (
    let () = log "got ident %S" i in
    return i)
;;

let string s = trace_pos (Format.sprintf "string `%s`" s) *> string s
let pattern = ws *> ident >>| pvar

let keyword kwd =
  ws
  *> string kwd
  *> let* c = peek_char_fail in
     if is_alpha c || is_digit c
     then fail "not a keyword"
     else ws *> return (log "keyword '%s' parsed" kwd)
;;

let prio expr table =
  let len = Array.length table in
  let rec helper level =
    if level >= len
    then expr
    else (
      let xs = table.(level) in
      return (fun h tl ->
          log "helper returned h ='%a'" Parsetree.pp_expr h;
          log "                tl size = %d" (List.length tl);
          List.fold_left (fun acc (op, r) -> op acc r) h tl)
      <*> helper (level + 1)
      <*> many
            (choice
               (List.map
                  (fun (op, f) -> op *> helper (level + 1) >>= fun r -> return (f, r))
                  xs)))
  in
  helper 0
;;

let letdef erhs =
  return (fun isrec name ps rhs -> isrec, name, List.fold_right elam ps rhs)
  <*> (keyword "let" *> option NonRecursive (keyword "rec" >>| fun _ -> Recursive) <* ws)
  <*> pattern
  <*> many pattern
  <*> ws *> keyword "=" *> ws *> erhs
;;

(* The equivalent of [letdef] *)
let letdef0 erhs =
  let+ isrec =
    keyword "let" *> option NonRecursive (keyword "rec" >>| fun _ -> Recursive) <* ws
  in
  let+ name = pattern in
  let+ ps = many pattern in
  let+ rhs = ws *> keyword "=" *> ws *> erhs in
  isrec, name, List.fold_right elam ps rhs
;;

let pack : dispatch =
  let prio d =
    prio
      (d.expr_long d)
      [| [ ws *> string "=", eeq ]
       ; [ ws *> string "+", eadd; string "-", esub ]
       ; [ ws *> string "*", emul ]
      |]
  in
  let expr d =
    fix (fun _self ->
        ws
        *> (parens (d.prio d)
           <|> (ws *> number >>| econst)
           <|> (ws *> ident >>| evar)
           <|> (keyword "fun" *> pattern
               >>= fun p ->
               ws *> string "->" *> ws *> d.prio d >>= fun b -> return (elam p b))
           <|> (keyword "if" *> d.prio d
               >>= fun cond ->
               keyword "then" *> d.prio d
               >>= fun th ->
               keyword "else" *> d.prio d >>= fun el -> return (eite cond th el))
           <|> (letdef (d.prio d)
               >>= fun (isrec, ident, rhs) ->
               keyword "in" *> d.prio d >>= fun in_ -> return (elet ~isrec ident rhs in_)
               )))
  in
  let expr_long d =
    fix (fun _self ->
        many (d.expr d)
        >>= function
        | [] -> fail "can't parse many expressions"
        | [ h ] -> return h
        | foo :: args -> return @@ eapp foo args)
  in
  { expr; expr_long; prio }
;;

let parse_pack p str = parse_string ~consume:All (p pack) str

let parse str =
  Caml.Format.printf "parsing a string '%s'\n%!" str;
  parse_pack pack.prio str
;;
