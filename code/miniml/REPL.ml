open Base
module Format = Caml.Format
open Miniml

let run_repl _ = failwith "not implemented"

let run_single () =
  let text = Stdio.In_channel.(input_all stdin) |> String.rstrip in
  let ast = Parsing.parse text in
  match ast with
  | Error s -> Format.printf "Error: %s\n%!" s
  | Result.Ok ast ->
    Format.printf "Parsed: %a\n%!" Parsetree.pp_expr ast;
    (match Inferencer.w ast with
    | Result.Ok ty -> Format.printf "Result: %a\n%!" Pprint.pp_typ ty
    | Result.Error e -> Format.printf "Error: %a" Inferencer.pp_error e)
;;

type opts = { mutable batch : bool }

let () =
  let opts = { batch = false } in
  Caml.Arg.parse
    [ "-", Caml.Arg.Unit (fun () -> opts.batch <- true), "read from stdin" ]
    (fun _ -> assert false)
    "TODO";
  (if opts.batch then run_single else run_repl) ()
;;
