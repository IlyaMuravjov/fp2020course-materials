let n2 = (261_793 * 2) + 78

let rec make depth =
  if depth <= 0 then []
  else
    let r = make (depth - 1) in
    1 :: r

let size = List.length

let _ =
  (* Gives stack overflow *)
  let wrap n =
    Format.printf "depth = %d, size_tail = %s\n%!" n
      ( try string_of_int @@ size (make n)
        with Stack_overflow -> "<stack overflow>" ) in
  wrap n2
