(* All this will become less relevant when tail-rec-modulo-constructor optimization will be in compiler *)
let wrap f n =
  Format.printf "depth = %d, size_tail = %s\n%!" n
    (try f n with Stack_overflow -> "<stack overflow>")

module _ = struct
  let n =
    match Sys.backend_type with
    | Sys.Bytecode -> 262_074
    | _ -> failwith "not implemented"

  let rec make depth =
    if depth <= 0 then []
    else
      let r = make (depth - 1) in
      1 :: r

  let size = List.length

  let _ =
    print_endline "lists";
    wrap (fun n -> string_of_int @@ size (make n)) n;
    (* Gives stack overflow *)
    wrap (fun n -> string_of_int @@ size (make n)) (n + 1);
    ()
end

module _ = struct
  let depth =
    match Sys.backend_type with
    | Sys.Bytecode -> 174_716
    | _ -> failwith "not implemented"

  type tree = Leaf | Node of tree * tree

  let rec make depth =
    if depth <= 0 then Leaf
    else
      let r = make (depth - 1) in
      let l = if depth mod 100000 = 0 then r else Leaf in
      Node (l, r)

  let size root =
    let rec helper tree =
      match tree with Leaf -> 0 | Node (l, r) -> helper l + helper r + 1 in
    helper root

  let () =
    print_endline "non-tailrec trees";
    wrap (fun n -> string_of_int @@ size (make n)) depth;
    wrap (fun n -> string_of_int @@ size (make n)) (1 + depth);
    ()

  let make_tail depth =
    let rec helper acc n =
      if depth < n then acc
      else
        let l = if n mod 100000 = 0 then acc else Leaf in
        helper (Node (l, acc)) (n + 1) in
    helper Leaf 1

  (* CPS transformation *)
  let size_tail root =
    let rec helper tree k =
      match tree with
      | Leaf -> k 0
      | Node (l, r) ->
          (helper [@tailcall]) l (fun sl ->
              (helper [@tailcall]) r (fun sr -> k (sl + sr + 1)) ) in
    helper root (fun n -> n)

  let () =
    print_endline "Should work correctly twice";
    wrap (fun n -> string_of_int @@ size_tail (make_tail n)) depth;
    wrap (fun n -> string_of_int @@ size_tail (make_tail n)) (depth + 1);
    ()
end

(*
let __ () =
  let n = 100000 in
  Format.printf "Tree: depth = %d, size = %d\n%!" n (size (make n));
  let n = 1000000 in
  (* Gives stack overflow *)
  (* Format.printf "Tree: depth = %d, size_tail = %d\n%!" n (size (make n)); *)
  (* WTF it gives stack overflow? *)
  Format.printf "Tree: depth = %d, size_tail = %d\n%!" n (size_tail (make n));
  ()
 *)
