(* Histomorphisms *)

module FibonacciHisto = struct
  type nat = Z | S of nat

  (* https://stackoverflow.com/a/24892711/1065436  *)
  let histo : ('a list -> 'a) -> nat -> 'a =
   fun f ->
    let rec go : nat -> 'a list = function
      | Z -> [f []]
      | S x ->
          let subvalues = go x in
          f subvalues :: subvalues in
    fun x -> List.hd (go x)

  (* -- Example: calculate the n-th fibonacci number *)
  let fibN : nat -> int = histo (function x :: y :: _ -> x + y | _ -> 1)

  let%test _ = fibN (S Z) = 1
  let%test _ = fibN (S (S Z)) = 2
  let%test _ = fibN (S (S (S Z))) = 3
  let%test _ = fibN (S (S (S (S Z)))) = 5
end

let rec cata fmap phi x = phi (fmap (cata fmap phi) x)

(* hylo === fold after unfold === cata after ana *)

(* dyno === hist after unfold *)

(* Но Хистоморфизм выразим через катаморфизм, поэтому
   динамическое программирование мы будем показывать
   на хиломорфизмах, а не диноморфизмах *)

(* Примеры из статьи: "Recursion schemes for dynamic programming" *)

module FibHylo : sig
  type 'a inter

  val psi : int -> int inter
  val phi : int inter -> int
  val int_unfold : (('a -> 'b) -> 'c -> 'b) -> ('a -> 'c) -> 'a -> 'b
  val hylo : ('a inter -> 'a) -> ('b -> 'b inter) -> 'b -> 'a
  val fib : int -> int
end = struct
  type 'a inter = Zero | One | Two of 'a * 'a [@@deriving map]

  let psi : int -> int inter = function
    | 0 -> Zero
    | 1 -> One
    | n -> Two (n - 1, n - 2)

  let phi = function Zero -> 0 | One -> 1 | Two (x, y) -> x + y

  let int_unfold fmap f =
    let rec helper n = fmap helper (f n) in
    helper

  let (_ : int -> ('a inter as 'a)) = int_unfold map_inter psi
  let hylo phi psi x = cata map_inter phi (int_unfold map_inter psi x)
  let fib = hylo phi psi

  let%test _ = fib 0 = 0
  let%test _ = fib 1 = 1
  let%test _ = fib 2 = 1
  let%test _ = fib 3 = 2
  let%test _ = fib 4 = 3
end

(* Binary partitions: количество способов преставить (без повторений)
   число в виде суммы степеней двойки *)
module BP : sig
  type 'a inter

  val psi : int -> int inter
  val phi : int inter -> int
  val hylo : ('a inter -> 'a) -> ('b -> 'b inter) -> 'b -> 'a
  val count : int -> int
end = struct
  type 'a inter = No | One of 'a | Two of 'a * 'a [@@deriving map]

  let rec unfold fmap f n = fmap (unfold fmap f) (f n)
  let hylo phi psi x = cata map_inter phi (unfold map_inter psi x)

  let psi : int -> int inter = function
    | 0 -> No
    | n when n mod 2 = 1 -> One (n - 1)
    | n -> Two (n - 1, n / 2)

  let phi = function No -> 1 | One x -> x | Two (x, y) -> x + y
  let count = hylo phi psi

  let%test _ = (* 0 = 0 *) count 0 = 1
  let%test _ = (* 1 = 2^0 *) count 1 = 1
  let%test _ = (* 2 = 2^1 = 2^0 + 2^0 *) count 2 = 2
  let%test _ = (* 3 = 2^1 + 2^0 = 2^0 + 2^0 + 2^0 *) count 3 = 2
end

module LongestCommonSubsequence : sig
  type ('a, _) inter

  val psi : 'a list * 'a list -> ('a, 'a list * 'a list) inter
  val phi : ('a, 'a list) inter -> 'a list
  val hylo : (('a, 'b) inter -> 'b) -> ('c -> ('a, 'c) inter) -> 'c -> 'b
  val lcs2 : int list * int list -> int list
end = struct
  (* direct style *)
  let lcs xs ys =
    let rec helper = function
      | [], _ -> []
      | _, [] -> []
      | x :: xs, y :: ys when x = y -> x :: helper (xs, ys)
      | x :: xs, y :: ys ->
          let r1 = helper (x :: xs, ys) in
          let r2 = helper (xs, y :: ys) in
          if List.length r1 > List.length r2 then r1 else r2 in
    helper (xs, ys)

  let%test _ = lcs [0; 1; 2; 3] [1; 2; 3; 4] = [1; 2; 3]

  type ('a, 'self) inter = No | One of ('a * 'a) * 'self * 'self * 'self
  [@@deriving map]

  (* Our new type is two-paramteric now *)
  let fmap f = map_inter Fun.id f

  let psi = function
    | [], _ | _, [] -> No
    | x :: xs, y :: ys -> One ((x, y), (xs, y :: ys), (x :: xs, ys), (xs, ys))

  let unfold fmap f =
    let rec helper n = fmap helper (f n) in
    helper

  let phi = function
    | No -> []
    | One ((a, b), _, _, x3) when a = b -> a :: x3
    | One ((_, _), x1, x2, _) when List.length x1 > List.length x2 -> x1
    | One ((_, _), _, x2, _) -> x2

  let hylo phi psi x = cata (map_inter Fun.id) phi (unfold fmap psi x)
  let lcs2 = hylo phi psi

  let%test _ = [1; 2; 3] = lcs2 ([0; 1; 2; 3], [1; 2; 3; 4])
end
