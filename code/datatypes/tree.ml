module type S = sig
  type elem

  type t

  val empty : t

  val add : elem -> t -> t

  val member : elem -> t -> bool
end

module type ORDERED = sig
  type t

  val compare : t -> t -> int
end

module UnbalancedSet (Element : ORDERED) : sig
  type elem = Element.t

  type t = private Empty | Tree of t * elem * t

  include S with type elem := elem and type t := t
end = struct
  type elem = Element.t

  type t = Empty | Tree of t * elem * t

  let empty = Empty

  let rec member x = function
    | Empty -> false
    | Tree (l, y, r) -> (
        match Element.compare x y with
        | 0 -> true
        | n when n < 0 -> member x l
        | _ -> member x r )

  let rec add x = function
    | Empty -> Tree (Empty, x, Empty)
    | Tree (l, y, r) as s -> (
        match Element.compare x y with
        | 0 -> s
        | n when n < 0 -> Tree (add x l, y, r)
        | _ -> Tree (l, y, add x r) )
end

module Strings = UnbalancedSet (String)

let show_tree (root : Strings.t) =
  let filename = "/tmp/demo.dot" in
  let ch = open_out filename in
  let ppf = Format.formatter_of_out_channel ch in
  let rec helper parent tree =
    match tree with
    | Strings.Empty -> ()
    | Tree (l, s, r) ->
        Format.fprintf ppf "%s [label=\"%s\"];\n" s s;
        Format.fprintf ppf "%s -> %s;" parent s;
        helper s l;
        helper s r
  in
  let () =
    match root with
    | Empty -> ()
    | Tree (l, v, r) ->
        Format.fprintf ppf "digraph graphname {\n";
        Format.fprintf ppf "root [label=\"%s\"];\n" v;
        helper "root" l;
        helper "root" r;
        Format.fprintf ppf "}\n%!"
  in
  close_out ch;
  let _ =
    Sys.command
      (Format.asprintf "dot -Tpng %s -o /tmp/demo.png "
         filename)
  in
  let _ =
    Sys.command (Format.asprintf "xdg-open /tmp/demo.png ")
  in
  ()

let () =
  let set = Strings.empty in
  let set =
    Strings.(set |> add "h" |> add "g" |> add "f")
  in
  show_tree set;
  let set =
    Strings.(set |> add "e" |> add "d" |> add "c")
  in
  show_tree set;
  let set =
    Strings.(set |> add "x" |> add "y" |> add "z")
  in
  show_tree set
