open Queues

let repetitions = 100
let n, m = 50, 50
let results = Array.init (n + m) (fun _ -> [| 0.; 0.; 0. |])

module Test (Q : QUEUE) = struct
  let test save =
    Gc.compact ();
    let q = Q.of_list (List.init n Fun.id) in
    let q = List.fold_left Q.snoc q (List.init m Fun.id) in
    let rec loop i q =
      if Q.is_empty q
      then ()
      else (
        let c = Mtime_clock.counter () in
        let q2 = Q.tail q in
        let () =
          for _ = 1 to repetitions do
            ignore (Q.tail q)
          done
        in
        let span = Mtime.Span.to_ns (Mtime_clock.count c) in
        save i span;
        loop (i + 1) q2)
    in
    loop 0 q
  ;;
end

let report filename =
  let ch = open_out filename in
  let ppf = Format.formatter_of_out_channel ch in
  for i = 0 to n + m - 1 do
    Format.fprintf
      ppf
      "%d, %5.1f, %5.1f, %5.1f\n"
      (i + 1)
      results.(i).(0)
      results.(i).(1)
      results.(i).(2)
  done;
  Format.fprintf ppf "%!";
  close_out ch
;;

let plot filename =
  let max =
    Array.fold_left (fun acc a -> max acc (max a.(0) (max a.(1) a.(2)))) min_float results
    |> string_of_float
  in
  let min =
    Array.fold_left (fun acc a -> min acc (min a.(0) (min a.(1) a.(2)))) max_float results
    |> string_of_float
  in
  let open Format in
  let plot_file = "/tmp/plot.plot" in
  let text =
    [%string
      {asdf|
set grid
set title 'Three queues'
set yrange [$min:$max]
set xlabel 'Element in queue left'
#set label 'finished walk' at 15, 140
#unset label
#set label 'finished walk' at 15, 105
set logscale y
plot '$filename' u 1:2 w lp t 'Batched', '$filename' u 1:3 w lp t 'Bankers', '$filename' u 1:4 w lp t 'RealTime'
|asdf}]
  in
  Stdio.Out_channel.with_file plot_file ~f:(fun ch ->
      output_string ch text;
      flush ch;
      close_out ch);
  let (_ : int) = Sys.command (sprintf "gnuplot %s -p" plot_file) in
  ()
;;

let () =
  let () =
    let module T = Test (BatchedQueue) in
    T.test (fun i s -> results.(i).(0) <- s)
  in
  let () =
    let module T = Test (BankersQueue) in
    T.test (fun i s -> results.(i).(1) <- s)
  in
  let () =
    let module T = Test (RealTimeQueue) in
    T.test (fun i s -> results.(i).(2) <- s)
  in
  let csv_file = "/tmp/rez.csv" in
  report csv_file;
  plot csv_file
;;
