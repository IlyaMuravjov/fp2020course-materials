open Angstrom
open Format

module Lam = struct
  type t = Var of char | App of t * t | Abs of char * t
  [@@deriving show {with_path= false}]

  let var x = Var x
  let abs x l = Abs (x, l)
  let app l r = App (l, r)
end

let is_space = function ' ' | '\t' -> true | _ -> false
let spaces = skip_while is_space
let varname = satisfy (function 'a' .. 'z' -> true | _ -> false)

let conde = function
  | [] -> fail "empty conde"
  | h :: tl -> List.fold_left ( <|> ) h tl

type dispatch =
  {apps: dispatch -> Lam.t Angstrom.t; single: dispatch -> Lam.t Angstrom.t}

let parse_lam =
  let single pack =
    fix (fun _ ->
        conde
          [ char '(' *> pack.apps pack <* char ')'
          ; ( (string "λ" <|> string "\\") *> spaces *> varname
            <* spaces <* char '.'
            >>= fun var -> pack.apps pack >>= fun b -> return (Lam.Abs (var, b))
            ); (varname <* spaces >>= fun c -> return (Lam.Var c)) ] ) in
  let apps pack =
    many1 (spaces *> pack.single pack <* spaces)
    >>= function
    | [] -> fail "bad syntax"
    | x :: xs -> return @@ List.fold_left (fun l r -> Lam.App (l, r)) x xs in
  {single; apps}

let parse_optimistically str =
  Result.get_ok
  @@ Angstrom.parse_string (parse_lam.apps parse_lam) str
       ~consume:Angstrom.Consume.All

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "x y");
  [%expect {| App (Var (x), Var (y)) |}]

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "(x y)");
  [%expect {| App (Var (x), Var (y)) |}]

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "(\\x . x x)");
  [%expect {| Abs (x, App (Var (x), Var (x))) |}]

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "(λf.λx. f (x x))");
  [%expect {| Abs (f, Abs (x, App (Var (f), App (Var (x), Var (x))))) |}]

module PP = struct
  let rec pp ppf = function
    | Lam.Var c -> Format.fprintf ppf "%c" c
    | App (l, r) -> Format.fprintf ppf "(%a %a)" pp l pp r
    (*
    | App (l, r) ->
        Format.fprintf ppf "%a %a" pp l pp r (* Buggy implementation *)
        *)
    | Abs (x, b) -> Format.fprintf ppf "(\\%c . %a)" x pp b
end

module TestQCheck = struct
  open QCheck.Gen

  let varname = map Char.chr (int_range (Char.code 'a') (Char.code 'z'))

  let lam_gen =
    QCheck.Gen.(
      sized
      @@ fix (fun self n ->
             match n with
             | 0 -> map Lam.var varname
             | n ->
                 frequency
                   [ (1, map2 Lam.abs varname (self (n / 2)))
                   ; (1, map2 Lam.app (self (n / 2)) (self (n / 2))) ] ))

  let __ () =
    List.iter
      (fprintf std_formatter "%a\n" PP.pp)
      (QCheck.Gen.generate ~n:20 lam_gen)

  let arbitrary_lam =
    let open QCheck.Iter in
    let rec shrink_lam = function
      | Lam.Var i -> QCheck.Shrink.char i >|= Lam.var
      | Abs (c, b) -> of_list [b] <+> (shrink_lam b >|= fun b' -> Lam.abs c b')
      | App (a, b) ->
          of_list [a; b]
          <+> (shrink_lam a >|= fun a' -> Lam.app a' b)
          <+> (shrink_lam b >|= fun b' -> Lam.app a b') in
    QCheck.make lam_gen ~print:(asprintf "%a" PP.pp) ~shrink:shrink_lam

  let print_parse_is_identity =
    QCheck.(
      Test.make arbitrary_lam (fun l ->
          Result.ok l
          = Angstrom.parse_string ~consume:Consume.Prefix
              (parse_lam.single parse_lam)
              (Format.asprintf "%a" PP.pp l) ))

  let run () = QCheck_runner.run_tests [print_parse_is_identity]
end
